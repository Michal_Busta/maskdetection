FROM ubuntu:18.04
LABEL maintainer michal.busta@gmail.com

RUN apt-get update
RUN echo "America/Los_Angeles" > /etc/timezone
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install tzdata

RUN apt-get install -y --no-install-recommends \
        build-essential \
        cmake \
        git \
        wget \
        libatlas-base-dev \
        libboost-all-dev \
        libgflags-dev \
        libgoogle-glog-dev \
        libhdf5-serial-dev \
        libleveldb-dev \
        liblmdb-dev \
        libopencv-dev \
        libprotobuf-dev \
        libsnappy-dev \
        protobuf-compiler \
        caffe-cpu \
        libcaffe-cpu-dev \
        python-dev \
        python-numpy \
        python-pip \
        python-setuptools \
        python-scipy && \
    rm -rf /var/lib/apt/lists/*


WORKDIR /workspace

RUN git clone https://Michal_Busta@bitbucket.org/Michal_Busta/maskdetection.git . && \
    mkdir build && cd build && \
    cmake .. && \
    make


WORKDIR /workspace/build/bin
